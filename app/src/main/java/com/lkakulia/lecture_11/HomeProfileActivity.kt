package com.lkakulia.lecture_11

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class HomeProfileActivity : AppCompatActivity() {

    companion object {
        private val REQUEST_CODE = 99
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {

        //change profile button listener
        changeProfileButton.setOnClickListener({

            //get data from the page
            val firstname = firstnameTextView.text.toString()
            val lastname = lastnameTextView.text.toString()
            val email = emailTextView.text.toString()
            val birthYear = birthYearTextView.text.toString()
            val gender = genderTextView.text.toString()

            //initialize user instance
            val user = UserModel(firstname, lastname,email, birthYear, gender)

            //start activity with intent
            val intent = Intent(this, ChangeProfileActivity::class.java)
            intent.putExtra("userInfo", user)
            startActivityForResult(intent, REQUEST_CODE)

        })
    }

    //toast message function
    private fun toastMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT)
    }

    //on activity result function
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val user = data?.getParcelableExtra<UserModel>("userInfo")

            //update user info on the page
            firstnameTextView.text = user?.firstname
            lastnameTextView.text = user?.lastname
            emailTextView.text = user?.email
            birthYearTextView.text = user?.birthYear
            genderTextView.text = user?.gender
        }
    }

}
