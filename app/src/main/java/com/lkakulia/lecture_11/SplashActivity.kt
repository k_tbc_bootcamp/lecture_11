package com.lkakulia.lecture_11

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class SplashActivity : AppCompatActivity() {

    private lateinit var handler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        init()
    }

    private fun init() {
        handler = Handler()
    }

    //on start function with 5s delay
    override fun onStart() {
        super.onStart()
        handler.postDelayed(runnable, 5000)
    }

    //on pause function to remove call back
    override fun onPause() {
        super.onPause()
        handler.removeCallbacks(runnable)
    }

    private val runnable = Runnable({
        openHomeProfileActivity()
    })

    //open home profile activity function to go to the main activity
    private fun openHomeProfileActivity() {
        val intent = Intent(this, HomeProfileActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)

        startActivity(intent)
    }
}
