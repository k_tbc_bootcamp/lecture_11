package com.lkakulia.lecture_11

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_change_profile.*

class ChangeProfileActivity : AppCompatActivity() {

    companion object {
        private val REQUEST_CODE = 99
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_profile)

        init()
    }

    private fun init() {
        val user = intent.getParcelableExtra<UserModel>("userInfo")

        //set empty edit texts with the user data to edit
        firstnameEditText.setText(user?.firstname)
        lastnameEditText.setText(user?.lastname)
        emailEditText.setText(user?.email)
        birthYearEditText.setText(user?.birthYear)
        genderEditText.setText(user?.gender)

        //save profile button listener
        saveProfileButton.setOnClickListener({
            if (firstnameEditText.text.isEmpty() || lastnameEditText.text.isEmpty() ||
                emailEditText.text.isEmpty() || birthYearEditText.text.isEmpty() ||
                genderEditText.text.isEmpty())
                toastMessage("Please fill in all the fields")

            else {
                //save the user data
                user?.firstname = firstnameEditText.text.toString()
                user?.lastname = lastnameEditText.text.toString()
                user?.email = emailEditText.text.toString()
                user?.birthYear = birthYearEditText.text.toString()
                user?.gender = genderEditText.text.toString()

                //go to the main activity with the user data
                val intent = Intent(this, HomeProfileActivity::class.java)
                intent.putExtra("userInfo", user)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        })
    }

    //toast message function
    private fun toastMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}
